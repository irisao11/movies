CREATE TABLE movies(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    duration INT NOT NULL,
    movie_language VARCHAR(3) NOT NULL,
    release_date DATE NOT NULL,
    release_country VARCHAR(20) NOT NULL
    );
CREATE TABLE actors(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(40) NOT NULL,
    gender VARCHAR(1)
    );
CREATE TABLE directors(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL
    );
CREATE TABLE ratings(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    rating INT NOT NULL,
    comment VARCHAR(100)
    );
ALTER TABLE ratings
	ADD movie_id INT NOT NULL AFTER id,
	ADD	reviewer_id INT NOT NULL AFTER rating;
ALTER TABLE ratings
	ADD FOREIGN KEY (movie_id) REFERENCES movies(id);
ALTER TABLE ratings
	ADD FOREIGN KEY(reviewer_id) REFERENCES reviewers(id);
ALTER TABLE ratings
	ADD UNIQUE KEY id_movie_reviewer(movie_id, reviewer_id);
CREATE TABLE genres(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    movie_type VARCHAR(50) NOT NULL
    );
 
 CREATE TABLE movies_actors(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    movie_id INT NOT NULL,
    actors_id INT NOT NULL
    );
ALTER TABLE movies_actors
	ADD FOREIGN KEY(movie_id) REFERENCES movies(id),
    ADD FOREIGN KEY(actors_id) REFERENCES actors(id);
ALTER TABLE movies_actors
	ADD UNIQUE KEY id_movies_actors(movie_id, actors_id);
CREATE TABLE movies_directors(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    movie_id INT NOT NULL,
    director_id INT NOT NULL,
    director_type VARCHAR(25) NOT NULL
    );
ALTER TABLE movies_directors
		ADD FOREIGN KEY(movie_id) REFERENCES movies(id),
        ADD FOREIGN KEY(director_id) REFERENCES directors(id);
ALTER TABLE movies_directors
	ADD UNIQUE KEY id_movies_directors(movie_id, director_id);
CREATE TABLE movies_genres(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    movies_id INT NOT NULL,
    genre_id INT NOt NULL
    );
ALTER TABLE movies_genres
	ADD FOREIGN KEY(movies_id) REFERENCES movies(id),
    ADD FOREIGN KEY(genre_id) REFERENCES genres(id);
ALTER TABLE movies_genres
	ADD UNIQUE KEY id_movies_genres(movies_id, genre_id);
CREATE TABLE reviewers(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nickname VARCHAR(20) NOT NULL
    );