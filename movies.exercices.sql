SELECT * FROM movies_sda_sql.directors;
-- 1. Afisati numele si anul filmelor.
SELECT title, YEAR(release_date) FROM movies;
-- 2. Afisati anul cand filmul <insert_movie_name_here> a fost lansat.
SELECT YEAR(release_date) FROM MOVIES WHERE title = 'Star Wars';
-- 3. Afisati toate filmele lansate in anul <insert_year_here>.
SELECT title FROM movies WHERE YEAR(release_date) = '2003';
-- 4. Afisati toate filmele lansate inainte de anul <insert_year_here>.
SELECT * FROM movies WHERE YEAR(release_date) > '2003';
-- 5. Afisati toate filmele si toti reviewerii intr-o singura lista.
SELECT 
	movies.title,
    reviewers.nickname
		FROM movies
        INNER JOIN
			ratings ON movies.id = ratings.movie_id
		INNER JOIN
			reviewers ON ratings.reviewer_id = reviewers.id;
-- 6. Afisati toti reviewerii care au dat o nota mai mare mare de 3.
SELECT 
	reviewers.nickname,
    ratings.rating
		FROM 
        reviewers
        INNER JOIN
			ratings ON ratings.reviewer_id = reviewers.id
				WHERE ratings.rating > '3';
-- 7. Afisatii toate filmele care nu au rating deloc.
SELECT 
	movies.title
		FROM movies
			INNER JOIN
				ratings ON ratings.movie_id = movies.id
					WHERE ratings.movie_id = ratings.rating IS NULL;
-- 8. Afisati toate filmele din seria "Terminator".
SELECT * FROM movies WHERE title LIKE 'Terminator%';
-- 9. Afisati toti actorii care au jucat in filmul <insert_movie_name_here>.
SELECT 
	actors.first_name,
    actors.last_name
		FROM actors
			INNER JOIN
				movies_actors ON movies_actors.actors_id = actors.id
			INNER JOIN 
				movies ON movies_actors.movie_id = movies.id
                WHERE movies.title = 'Child Pose';
-- 10. Afisati toate filmele din afara USA.
SELECT * FROM movies WHERE release_country NOT IN('USA');
-- 11. Afisati toate filmele ale caror director a fost <insert_dir_name_here>. Folositi subquery.
SELECT title FROM movies WHERE id IN(SELECT movie_id FROM movies_directors WHERE director_id IN(SELECT id FROM directors WHERE first_name IN('James') AND last_name IN('CAMERON')));
-- 12. Afisati anii in care a fost produs macar 1 film a carui rating este de minim 3.
SELECT YEAR(release_date) FROM movies WHERE id IN(SELECT movie_id FROM ratings WHERE rating<'4');
-- 13. Afisati toti reviewerii care au dat note fara comentariu.
SELECT
	reviewers.nickname
		FROM reviewers
			INNER JOIN 
				ratings ON ratings.reviewer_id = reviewers.id
					WHERE ratings.comment IS NULL;
-- 14. Afisati descrescator notele si reviewerii pentru filmul <insert_movie_name_here>.
SELECT 
	movies.title,
    ratings.rating
		FROM movies
			INNER JOIN
				ratings ON ratings.movie_id = movies.id
					ORDER BY ratings.rating DESC; 
-- 15. Afisati toate filmele impreuna cu rating-ul lor in ordine descrescatoare.
SELECT ratings.rating,
	   reviewers.nickname
       FROM ratings
		INNER JOIN reviewers ON ratings.reviewer_id = reviewers.id
        INNER JOIN movies ON ratings.movie_id = movies.id
			WHERE movies.title = 'Child Pose'
            ORDER BY ratings.rating DESC, reviewer_id DESC;
-- 16. Afisati cei mai activi reviewers.
SELECT reviewer_id, COUNT(reviewer_id) FROM ratings GROUP BY reviewer_id ORDER BY COUNT(rating) DESC LIMIT 2;
-- 17. Afisati filmul cele mai bune rating-uri.
SELECT 
	movies.title,
    AVG(ratings.rating)
    FROM movies
    INNER JOIN
		ratings ON ratings.movie_id = movies.id
			GROUP BY movie_id ORDER BY AVG(ratings.rating) DESC  LIMIT 1;
-- 18. Afisati filmul cu cele mai multe rating-uri.
SELECT 
	movies.title,
    count(rating)
    FROM movies
		INNER JOIN ratings ON ratings.movie_id = movies.id
			GROUP BY movie_id ORDER BY COUNT(rating) DESC LIMIT 1;
-- 19. Afisati acele filme in care au jucat actori, actori ce au macar 2 filme la activ.
SELECT distinct
	movies.title
    FROM movies
		INNER JOIN
			movies_actors ON movies_actors.movie_id = movies.id
		INNER JOIN actors ON movies_actors.actors_id = actors.id
        WHERE actors.id in (SELECT movies_actors.actors_id FROM movies_actors GROUP BY movies_actors.actors_id HAVING COUNT(*) >=2);
-- 20. Afisati al 6 lea film in ordinea ratingului.
SELECT 
	movies.title 
    FROM movies
    INNER JOIN
		ratings ON ratings.movie_id = movies.id
			GROUP BY ratings.movie_id ORDER BY AVG(ratings.rating) DESC LIMIT 5,1;
-- 21. Afisati actorii care au jucat in filmele <insert 2 movies here>
SELECT 
	actors.first_name, 
    actors.last_name 
    FROM actors
		INNER JOIN
        movies_actors ON actors.id = movies_actors.actors_id
            INNER JOIN
            movies ON movies_actors.movie_id = movies.id
            WHERE title IN('Taxi', 'Amelie');

select a.first_name, a.last_name,a.id from actors a join movies_actors m on a.id = m.actors_id join 
movies on movies.id = m.movie_id where title IN('Taxi', 'Amelie');
        
        
        
			movies_actors ON actors.id = movies_actors.actors_id
            INNER JOIN
            movies ON movies_actors.movie_id = movies.id
            WHERE title IN('Taxi', 'Amelie');

select a.first_name, a.last_name,a.id from actors a join movies_actors m on a.id = m.actors_id join 
movies on movies.id = m.movie_id where title IN('Taxi', 'Amelie');